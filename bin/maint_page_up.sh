#!/bin/bash
# This script will bring up the erecruit maintenance page
if [ ! -L /etc/haproxy/haproxy.cfg ]
then
    echo "/etc/haproxy/haproxy.cfg is not a symlink! Stopping."
    exit -1
fi
rm /etc/haproxy/haproxy.cfg
ln -s /etc/haproxy/haproxy_maint.cfg /etc/haproxy/haproxy.cfg
ls -la /etc/haproxy/haproxy.cfg
service haproxy restart
